// METHOD 1

import java.lang.*;

public int randNumv2(int min, int max) {
        int number = (int) (Math.random() * (max - min + 1) + min);
        return number;
}

// METHOD 2

import java.util.Random;

// Select a random number between min and max included [min - max]
public int randNum(int min, int max) {
	int number = rand.nextInt((max - min) + 1) + min;
	return number;
}

